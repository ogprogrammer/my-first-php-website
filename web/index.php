<?php
/**
 * A simple example on how to create your first PHP web page.
 * @author Joshua Copeland <Josh@PsyCode.org>
 */

require '../vendor/autoload.php';

use PsyCodeDotOrg\Controller;

$controller = new Controller();
echo $controller->getResponse();