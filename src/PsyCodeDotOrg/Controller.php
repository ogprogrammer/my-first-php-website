<?php
namespace PsyCodeDotOrg;

use PsyCodeDotOrg\Model\Factory;
use PsyCodeDotOrg\View;

class Controller
{

    /**
     * @var string $uri The request uri
     */
    public $uri = '/';

    public function __construct($overrideUri = '')
    {
        if (!empty($overrideUri)) {
            $this->uri = $overrideUri;
        } else {
            $this->uri = $_SERVER['REQUEST_URI'];
        }
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        $response = null;
        switch ($this->uri) {
            case '/index.php':
            case '/':
            case '':
                $response = $this->getIndex();
                break;
            case '/create':
                $response = $this->getCreate();
                break;
            case '/phpinfo':
                $response = phpinfo();
                break;
            default:
                $response = '404';
        }
        return $response;
    }

    public function getIndex()
    {
        return View::renderMainView();
    }

    public function getCreate()
    {
        $factory = $this->getFactory();
        $params = $this->getRequestParameters();

        $object = $factory->getModel($this->getRequestParameters());
        $header = "<h1>Hello " . $object->getName() . "</h1>";
        $content = "<h3>Your Object</h3>" . print_r($object, true) . '<br />';
        $content .= "<h3>Request Parameters</h3>" . print_r($params, true);

        return View::renderMainView(View::TITLE, $header, $content, $params['name'], $params['age']);
    }


    private function getFactory()
    {
        return new Factory();
    }

    /**
     * @return array
     */
    public function getRequestParameters()
    {
        $params = array();

        if (isset($_REQUEST['name'])) {
            $params['name'] = htmlspecialchars($_REQUEST['name']);
        }
        if (isset($_REQUEST['age'])) {
            $params['age'] = (int)$_REQUEST['age'];
        }
        if (isset($_REQUEST['gender'])) {
            $params['gender'] = (int)$_REQUEST['gender'];
        }

        return $params;
    }
} 