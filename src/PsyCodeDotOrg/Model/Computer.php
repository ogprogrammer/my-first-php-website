<?php

namespace PsyCodeDotOrg\Model;

class Computer implements iUser
{
    private $name;

    /**
     * @param $name
     */
    public function __construct($name)
    {
        $this->setName($name);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @throws \InvalidArgumentException
     */
    private function setName($name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('Hey Kid, Name is required for computer. Stop all the downloading!');
        }
        $this->name = $name;
    }
} 