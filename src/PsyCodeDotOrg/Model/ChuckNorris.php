<?php

namespace PsyCodeDotOrg\Model;

final class ChuckNorris implements iUser
{
    const NAME = 'CHUCK NORRIS';

    public function __construct()
    {
        self::__destruct();
    }

    public function __destruct()
    {
        $this->roundhouse();
    }

    public static function roundhouse()
    {
        error_log('ROUNDHOUSE KICK, NOOOOOOOO!!! HOLY SHIIIIIIIII--');
        die();
    }

    public function getName()
    {
        return self::NAME;
    }
}