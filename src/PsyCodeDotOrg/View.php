<?php

namespace PsyCodeDotOrg;

class View
{

    private $title = "My First Website in PHP";
    private $header = "My First Website in PHP";
    const TITLE = "My First Website in PHP";
    const TEMPLATEFILEPATH = "../src/PsyCodeDotOrg/Resources/View/Template.html";
    const MAINMENU = "
    <nav id='mainmenu'>
        <a href='/'>Home</a> |
        <a href='/phpinfo'>phpinfo();</a>
    </nav>";

    public static function renderMainView(
        $title = "My First Website in PHP",
        $header = "<h1>My First Website in PHP</h1>",
        $content = "<h2>Welcome to the PHP object factory, type your name and age if you have one.</h2>",
        $name = "",
        $age = ""
   )
    {
        $view = file_get_contents(self::TEMPLATEFILEPATH);
        $view = str_replace("%%title%%", $title, $view);
        $view = str_replace("%%header%%", $header . self::MAINMENU, $view);
        $view = str_replace("%%content%%", $content, $view);
        $view = str_replace("%%name%%", $name, $view);
        $view = str_replace("%%age%%", $age, $view);
        $view = str_replace("%%currentyear%%", date('Y'), $view);
        return $view;
    }
} 