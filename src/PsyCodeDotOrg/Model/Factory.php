<?php

namespace PsyCodeDotOrg\Model;

use PsyCodeDotOrg\Model\ChuckNorris;
use PsyCodeDotOrg\Model\Chick;
use PsyCodeDotOrg\Model\SerjTankian;
use PsyCodeDotOrg\Model\Computer;
use PsyCodeDotOrg\Model\Dude;

class Factory
{
    public function getModel(array $parameters = null)
    {
        if (empty($parameters) || empty($parameters['name']) || !isset($parameters['gender'])) {
            throw new \InvalidArgumentException('Factory could not build model due to invalid arguments received.');
        }

        $name = strtoupper($parameters['name']);

        $gender = (bool)$parameters['gender'];

        // If age is set then it must be a MortalCreature
        if (!empty($parameters['age'])) {
            $age = $parameters['age'];
            // If gender is true, its a dude
            if ($parameters['gender']) {
                // Check for special cases
                if (strpos($name, 'SERJ') === false) {
                    return new Dude($parameters['name'], $age);
                } else {
                    return new SerjTankian($parameters['name'], $age);
                }
            } else {
                return new Chick($parameters['name'], $age);
            }
        } elseif (strpos($name, ChuckNorris::NAME) === false) {
            return new Computer($name);
        } else {
            return new ChuckNorris();
        }
    }
} 