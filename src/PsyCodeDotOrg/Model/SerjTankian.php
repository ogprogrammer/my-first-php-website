<?php

namespace PsyCodeDotOrg\Model;

class SerjTankian extends Dude
{
    public function __sleep()
    {
        return array('name','age','gender');
    }

    public function __wakeup()
    {
        // grab a brush and put a little makeup...
    }
} 