<?php

namespace PsyCodeDotOrg\Model;


abstract class MortalCreature implements iUser
{

    protected $name;
    protected $age;

    /**
     * @param $name
     * @param $age
     */
    public function __construct($name, $age)
    {
        $this->setName($name);
        $this->setAge($age);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param $name
     * @return mixed
     * @throws \InvalidArgumentException
     */
    protected function setName($name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException('Name is required.');
        }
        $this->name = $name;
    }

    /**
     * @param $age
     * @return mixed
     * @throws \InvalidArgumentException
     */
    protected function setAge($age)
    {
        if (empty($age)) {
            throw new \InvalidArgumentException('Age is required.');
        }
        $this->age = $age;
    }
} 