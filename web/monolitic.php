<html>
<head>
    <title>My First Website in PHP</title>
    <link rel="stylesheet" type="text/css" href="swag.css"/>
    <script src="script.js"></script>
</head>
<body>
<div class="wrapper">
    <header>
        <?php
        // Check $_POST for submitted personal information
        if (!empty($_POST['name']) && !empty($_POST['age'])) {

            // Casting to int strips any unwanted scripts/injection attempts, just casts to first int found.
            $age = (int)$_POST['age'];
            // Strings must always be checked and never trust user input!
            $name = htmlspecialchars($_POST['name']);

            // You can append strings with .
            $msg = '<h1>Aloha ' . $name . ', you are ' . $age;
            // Create

            //    if age is not 1  ?   true  :  false
            $msg .= ($age !== 1) ? ' years' : ' year' . ' old!</h1>';
            //   .= appends to the string


            echo $msg;

        } elseif (!empty($_POST['name'])) {
            echo '<h1>Aloha ' . htmlspecialchars($_POST['name']) . '.<h1>';
        } else {
            echo '<h1>Welcome to your first PHP web page.</h1>';
        }



        ?>
    </header>
    </header>
    <div class="container">
        <?php
        // See if the IE user agent string is anywhere in the user agent from web server
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE) {
            echo '<p>You are using Internet Explorer, you should get chrome or ff bro.</p>';
        } else {
            echo '<p>You are not using Internet Explorer</p>';
        }
        echo $_SERVER['HTTP_USER_AGENT'];
        ?>
        <br/>

        <form action="/index.php" method="post">
            <p>
                <label for="name">Name:</label>
                <input type="text" name="name" id="name"/>
            </p>

            <p>
                <label for="age">Age:</label>
                <input type="number" name="age" id="age"step="1" min="0" max="120"/>
            </p>
            <p>
                <input type="submit" value="Show me the $"/>
            </p>
        </form>
    </div>
    <footer>
        <ul>
            <li>My First PHP Website 2014/li>
            <li>| Free Beer License</li>
            <li>| <a href="http://www.php.net/">PHP.net</a></li>
            <li>| <a href="http://talks.php.net/">PHP Talks</a></li>
            <li>| <a href="http://www.w3schools.com/php/php_examples.asp">W3C PHP Examples</a></li>
        </ul>
        <img src="http://talks.php.net/images/animated_elephant.gif"/>
    </footer>
</div>
</body>
</html>